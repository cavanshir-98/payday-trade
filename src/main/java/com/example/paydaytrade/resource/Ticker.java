package com.example.paydaytrade.resource;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Calendar;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Ticker {
    private String symbol;
    private Calendar getLastTradeDateStr;
    private TickerStockQuote quote;
}
