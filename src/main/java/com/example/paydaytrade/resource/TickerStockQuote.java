package com.example.paydaytrade.resource;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class TickerStockQuote {

    private BigDecimal price;
}
