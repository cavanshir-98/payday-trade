package com.example.paydaytrade.wrapper;

import lombok.RequiredArgsConstructor;
import yahoofinance.quotes.stock.StockQuote;

import java.time.LocalDateTime;

@RequiredArgsConstructor
public class StockWrapper {

    private final StockQuote stockQuote;
    private final LocalDateTime lastAccess;

    public StockWrapper(StockQuote stock) {
        this.stockQuote = stock;
        this.lastAccess = LocalDateTime.now();
    }

    public LocalDateTime getLastAccess() {
        return lastAccess;
    }

    public StockQuote getStock() {
        return stockQuote;
    }

}