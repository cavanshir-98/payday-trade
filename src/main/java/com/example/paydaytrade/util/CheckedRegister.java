package com.example.paydaytrade.util;

import com.example.paydaytrade.dto.RegisterDto;
import com.example.paydaytrade.enums.RoleEnum;
import com.example.paydaytrade.exception.EmailAlreadyExistException;
import com.example.paydaytrade.exception.RoleNotFoundException;
import com.example.paydaytrade.model.Role;
import com.example.paydaytrade.repository.RoleRepository;
import com.example.paydaytrade.repository.UserRepository;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CheckedRegister {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;

    public void checked(RegisterDto registerDto) {

        Set<Role> roles = new HashSet<>();

        List<Role> roleList = roleRepository.findAll();
        if (roleList.isEmpty()) {
            roleRepository.save(Role.builder().name(RoleEnum.ADMIN).build());
            roleRepository.save(Role.builder().name(RoleEnum.USER).build());
        }
        if (userRepository.existsByEmail(registerDto.getEmail())) {
            throw new EmailAlreadyExistException();
        }
        if (registerDto.getAuthority().contains("ADMIN")) {
            Role admin = roleRepository.findByName(RoleEnum.ADMIN)
                    .orElseThrow(RoleNotFoundException::new);
            roles.add(admin);
        }
        if (registerDto.getAuthority().contains("USER")) {
            Role userRole = roleRepository.findByName(RoleEnum.USER)
                    .orElseThrow(RoleNotFoundException::new);
            roles.add(userRole);
        }
        if (!registerDto.getAuthority().contains("ADMIN")
                && !registerDto.getAuthority().contains("USER")) {
            throw new RoleNotFoundException();
        }
    }

    public CheckedRegister(UserRepository userRepository, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }
}
