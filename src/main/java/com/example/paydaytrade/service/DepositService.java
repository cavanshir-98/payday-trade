package com.example.paydaytrade.service;

import com.example.paydaytrade.dto.balance.BalanceRequest;

public interface DepositService {

    void addBalanceForUser(BalanceRequest balanceRequest, Long id);
}
