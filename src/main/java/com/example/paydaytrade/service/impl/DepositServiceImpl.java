package com.example.paydaytrade.service.impl;

import com.example.paydaytrade.dto.balance.BalanceRequest;
import com.example.paydaytrade.exception.NotFoundException;
import com.example.paydaytrade.model.Balance;
import com.example.paydaytrade.model.User;
import com.example.paydaytrade.repository.BalanceRepository;
import com.example.paydaytrade.repository.UserRepository;
import com.example.paydaytrade.service.DepositService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
public class DepositServiceImpl implements DepositService {

    private final BalanceRepository balanceRepository;
    private final UserRepository userRepository;

    @Override
    @Transactional
    public void addBalanceForUser(BalanceRequest balanceRequest, Long id) {

        Balance balance = new Balance();
        User user = userRepository.findById(id).orElseThrow(() -> new NotFoundException("User not found"));

        if (user.getBalance() == null) {
            user.setBalance(balance);
            userRepository.save(user);
        }

        if (user.getBalance().getCurrentBalance() == null) {
            balance.setCurrentBalance(0L);
            balanceRepository.save(balance);

        }

        Long balanceUser = user.getBalance().getCurrentBalance() + balanceRequest.getAddBalance();
        user.getBalance().setCurrentBalance(balanceUser);
        userRepository.save(user);
    }
}
