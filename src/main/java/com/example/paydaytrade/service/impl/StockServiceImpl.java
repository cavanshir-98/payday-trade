package com.example.paydaytrade.service.impl;

import com.example.paydaytrade.resource.Ticker;
import com.example.paydaytrade.resource.TickerStockQuote;
import com.example.paydaytrade.service.StockService;
import com.example.paydaytrade.wrapper.StockWrapper;
import org.springframework.stereotype.Service;
import yahoofinance.YahooFinance;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class StockServiceImpl implements StockService {

    public StockWrapper findStock(String symbol) {
        try {
            return new StockWrapper(YahooFinance.get(symbol).getQuote());
        } catch (RuntimeException | IOException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    public List<Ticker> findStocks() {

        List<String> stocks = new ArrayList<>();
        stocks.add("ADBE");
        stocks.add("AMD");
        stocks.add("AMZN");
        stocks.add("ABNB");
        stocks.add("ADI");
        stocks.add("AEP");

        List<Ticker> tickerList = new ArrayList<>();
        Ticker ticker;
        TickerStockQuote tickerStockQuote;
        List<StockWrapper> stockWrappers = stocks
                .stream()
                .map(this::findStock)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

        for (StockWrapper stockWrapper1 : stockWrappers) {
            ticker = new Ticker();
            tickerStockQuote = new TickerStockQuote();
            ticker.setQuote(tickerStockQuote);

            ticker.getQuote().setPrice(stockWrapper1.getStock().getPrice());
            ticker.setSymbol(stockWrapper1.getStock().getSymbol());
            ticker.setGetLastTradeDateStr(stockWrapper1.getStock().getLastTradeTime());
            tickerList.add(ticker);
        }
        return tickerList;
    }
}
