package com.example.paydaytrade.controller;

import com.example.paydaytrade.resource.Ticker;
import com.example.paydaytrade.service.StockService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/stocks")
public class StockController {

    private final StockService service;

    @PostMapping
    BigDecimal home() {
        return service.findStock("GOOG").getStock().getAsk();
    }

    @PostMapping("/getAll")
    List<Ticker> findAll() {
        return service.findStocks();
    }
}
