package com.example.paydaytrade.repository;

import com.example.paydaytrade.model.Balance;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BalanceRepository extends JpaRepository<Balance, Long> {
}
